using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;

public class VideoButtonTrigger : MonoBehaviour
{
    private bool isButtonShowing;
    private bool isVideoPlaying;
    [SerializeField] private VideoPlayer videoPlayer;
    [SerializeField] private Button playBtn;
    [SerializeField] private Button triggerBtn;
    [SerializeField] private GameObject buttonsGroup;
    [SerializeField] private Sprite[] playIcons;

    private void Start()
    {
        isButtonShowing = true;
        playBtn.image.sprite = playIcons[1];

        playBtn.onClick.AddListener(PlayToggle);
        triggerBtn.onClick.AddListener(OnTrigger);
    }

    public void SetButtonStatus(bool value)
    {
        isButtonShowing = value;
    }

    private void OnTrigger()
    {
        isButtonShowing = !isButtonShowing;
        ShowButtons(isButtonShowing);
    }

    private void ShowButtons(bool isButtonShowing)
    {
        if (!isButtonShowing)
        {
            buttonsGroup.SetActive(false);
        }
        else
        {
            buttonsGroup.SetActive(true);
        }
    }

    private void PlayToggle()
    {
        isVideoPlaying = !isVideoPlaying;
        playBtn.image.sprite = playIcons[isVideoPlaying ? 0 : 1];
        if (isVideoPlaying) videoPlayer.Pause();
        else videoPlayer.Play();

    }
}
