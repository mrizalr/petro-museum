using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LanguageToggle : MonoBehaviour
{
    public Button[] btn;
    [SerializeField] private bool isEn;
    private LanguageHandler[] languageHandlers;

    private void Start()
    {
        btn[0].onClick.AddListener(() => ChangeLanguage(false));
        btn[1].onClick.AddListener(() => ChangeLanguage(true));
        isEn = false;

        languageHandlers = FindObjectsOfType<LanguageHandler>(true);
        ChangeLanguage(false);

        // gameObject.SetActive(false);
        // transform.Find("Exit Confirmation").gameObject.SetActive(false);
    }

    private void ChangeLanguage(bool toEN)
    {
        btn[0].image.color = toEN ? new Color32(255, 255, 255, 255) : new Color32(244, 201, 2, 255);
        btn[1].image.color = toEN ? new Color32(244, 201, 2, 255) : new Color32(255, 255, 255, 255);
        foreach (var lh in languageHandlers)
        {
            lh.SetLanguage(toEN);
        }
    }
}
