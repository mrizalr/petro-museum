using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;

public class VideoMenuGenerator : MonoBehaviour
{
    [System.Serializable]
    public struct VideoData
    {
        public VideoClip clip;
        public Texture thumbnail;
    }

    [SerializeField] private List<VideoData> videoData;
    [SerializeField] private GameObject buttonPrefab;

    private VideoScreen videoScreen;

    private void Start()
    {
        videoScreen = FindObjectOfType<VideoScreen>();
        GenerateVideoThumbnail();
    }

    private void GenerateVideoThumbnail()
    {
        videoData.ForEach(data =>
        {
            var btn = CreateButton(data.clip.name);
            var videoPlayer = btn.GetComponentInChildren<VideoPlayer>();
            var btnTexture = btn.GetComponentInChildren<RawImage>();
            btn.GetComponent<Button>().onClick.AddListener(() => videoScreen.OpenScreen(data.clip));

            SetButtonVideo(videoPlayer, data.clip);
            btnTexture.texture = data.thumbnail;
            btn.SetActive(true);
        });
    }

    private GameObject CreateButton(string name)
    {
        var btn = Instantiate(buttonPrefab, this.transform);
        btn.name = name;
        return btn;
    }

    private void SetButtonVideo(VideoPlayer videoPlayer, VideoClip clip)
    {
        videoPlayer.clip = clip;
        videoPlayer.renderMode = VideoRenderMode.RenderTexture;

        var rt = CreateRenderTexture();
        rt.name = clip.name;
        rt.Create();
        videoPlayer.targetTexture = rt;
    }

    private RenderTexture CreateRenderTexture()
    {
        var button = buttonPrefab.GetComponent<RectTransform>();
        return new RenderTexture((int)button.rect.width, (int)button.rect.height, 24, RenderTextureFormat.ARGB64);
    }
}
