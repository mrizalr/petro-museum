using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using TMPro;
using AnotherFileBrowser.Windows;

public class BrowseFile : MonoBehaviour
{
    private TMP_InputField pathField;

    private void Start()
    {
        pathField = GetComponentInParent<TMP_InputField>();
    }

    public void Browse()
    {
        var bp = new BrowserProperties();
        bp.filter = "Executable files (*.exe) | *exe";
        bp.filterIndex = 0;

        new FileBrowser().OpenFileBrowser(bp, path => pathField.text = path);
    }
}
