using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonToggle : MonoBehaviour
{
    public void OnButtonToggle(GameObject target)
    {
        target.SetActive(!target.activeSelf);
    }
}
