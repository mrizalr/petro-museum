using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class MenuAction : MonoBehaviour
{
    public UnityEvent OnStartScenario;

    public void OpenApp(string path)
    {
        if (path.Length > 0) System.Diagnostics.Process.Start(path);
    }
}
