using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerButton : MonoBehaviour
{
    [SerializeField] private int maxClick;
    [SerializeField] private int clickCount;
    [SerializeField] private UnityEngine.Events.UnityEvent onTrigger;

    private Coroutine lastCoroutine;

    public void OnTriggerClick()
    {
        if (clickCount < maxClick - 1)
            lastCoroutine = StartCoroutine(OnClickCor());
        else
        {
            clickCount = 0;
            StopCoroutine(lastCoroutine);
            onTrigger.Invoke();
        }
    }

    public IEnumerator OnClickCor()
    {
        if (lastCoroutine != null) StopCoroutine(lastCoroutine);
        clickCount++;
        yield return new WaitForSeconds(.5f);
        clickCount = 0;
    }
}
