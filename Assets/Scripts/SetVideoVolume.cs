using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;

public class SetVideoVolume : MonoBehaviour
{
    [SerializeField] private VideoPlayer videoPlayer;
    private Slider slider;
    private void Start()
    {
        slider = GetComponent<Slider>();
        slider.value = videoPlayer.GetDirectAudioVolume(0);
    }

    public void SetVolume(float value)
    {
        videoPlayer.SetDirectAudioVolume(0, value);
    }
}
