using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class HideButton : MonoBehaviour
{
    [SerializeField] private float timeForDestruct;
    [SerializeField] private UnityEvent OnHideButton;
    private Coroutine currentCoroutine;

    private void OnEnable()
    {
        currentCoroutine = StartCoroutine(SelfDestructButton());
    }

    public void ResetTimer()
    {
        StopCoroutine(currentCoroutine);
        currentCoroutine = StartCoroutine(SelfDestructButton());
    }

    private IEnumerator SelfDestructButton()
    {
        yield return new WaitForSeconds(timeForDestruct);
        gameObject.SetActive(false);
        if (OnHideButton != null) OnHideButton.Invoke();
    }
}
