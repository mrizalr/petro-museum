using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class ScenarioSelected : MonoBehaviour
{
    public int currentSelected = 0;
    private Button[] scenarioButtons;

    private void Start()
    {
        scenarioButtons = GetComponentsInChildren<Button>();
        for (int i = 0; i < scenarioButtons.Length; i++)
        {
            var idx = i;
            scenarioButtons[idx].onClick.AddListener(() => ChangeSelectedButton(idx));
        }
    }

    private void ChangeSelectedButton(int index)
    {
        if (index == currentSelected) return;

        scenarioButtons[index].transform.DOScale(.9f, .5f);
        scenarioButtons[currentSelected].transform.DOScale(.7f, .5f);
        StartCoroutine(RefreshLayout(scenarioButtons[currentSelected].GetComponentInParent<HorizontalLayoutGroup>()));

        currentSelected = index;
    }

    private IEnumerator RefreshLayout(HorizontalLayoutGroup layout)
    {
        var count = 0f;
        while (count < 0.5f)
        {
            layout.CalculateLayoutInputVertical();
            layout.CalculateLayoutInputHorizontal();
            layout.SetLayoutVertical();
            layout.SetLayoutHorizontal();
            count += .01f;
            yield return new WaitForSeconds(.01f);
        }
    }
}
