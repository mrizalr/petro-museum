using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CanvasGroup))]
public class CanvasGroupHandler : MonoBehaviour
{
    private CanvasGroup _cg;

    private void Awake()
    {
        _cg = GetComponent<CanvasGroup>();
    }

    public void SetCanvas(bool active)
    {
        _cg.alpha = active ? 1 : 0;
        _cg.interactable = active;
        _cg.blocksRaycasts = active;
    }
}
